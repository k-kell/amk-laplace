#--------------------------------------------------------------------------
# FINITE ELEMENT ANALYSIS - 2D Laplace problems         
#                                                       
#               Isoparametric (3-9 nodes)               
#--------------------------------------------------------------------------
# Χρειάζεται τις functions: 1) Matrices_2D_Laplace      
#                           2) PGAUSS                   
#                           3) shape8                   
#--------------------------------------------------------------------------
# fixed_dofs     : Οι περιορισμένοι Βαθμοί Ελευθερίας
# free_dofs      : Οι μη-περιορισμένοι Βαθμοί Ελευθερίας
# F1             : διάνυσμα καθορισμένων τιμών στο σύνορο
#-------------------------------------------------------------------------
import numpy as np
from scipy import sparse as sp
from scipy.sparse.linalg import spsolve
import matplotlib
from matplotlib import pyplot as plt
from PGauss import PGauss
from MeshGen_reusto import MeshGen
#===    ΠΡΟ-ΕΠΕΞΕΡΓΑΣΤΗΣ (Δεδομένα του προβλήματος):

NODES=14     # Πλήθος κόμβων
NEL=3                  # Πλήθος κόμβων ανά στοιχείο
# Ορισμός και προετοιμασία (μηδενισμός) μητρώων και διανυσμάτων:
NDF = 1                     # αριθμός ΒΕ ανά κόμβο (1 DOFs per node)
NDOFS = NODES * NDF         # συνολικός αριθμός ΒΕ
#
X    = np.zeros([NODES,2])              # κομβικές συντεταγμένες (nodal co-ordinates)
#IX   = np.zeros([NELE,NEL]).astype('int') # τοπολογία στοιχείων (element topology IndeX)
BC   = np.zeros(NODES)              # οριακές συνθήκες (boundary conditions)
FI   = np.zeros(NODES,dtype=float)  # externally applied forces 
U    = np.zeros([1,NODES])              # αρχικές μετατοπίσεις
#-------------------------------------------------------------------------   
# Κομβικές συντεταγμένες:
X[:,0]=np.array([0 ,1 ,2-np.sqrt(2)/2 ,2 ,0 ,1 ,2-np.sqrt(2)/2 ,2 ,0 ,1 ,0, 1, 2-np.sqrt(2)/2, 2])
X[:,1]=np.array([0 ,0 ,np.sqrt(2)/2 ,1 ,np.sqrt(2)/2 ,np.sqrt(2)/2 ,1.5 ,1.5 ,1.5 ,1.5 ,2 ,2, 2, 2])
    
#for i in range(NR+1):
 #   nident=(NTH+1)*i
  #  plt.plot(X[nident:NTH+1+nident,0],X[nident:NTH+1+nident,1],color='blue',linewidth=1)
#plt.grid()
#nc=-1
#for j in range(NR):
    #for i in range(NTH):
        #nc+=1
        #IX[nc,0]=j*(NTH+1)+i+1 
       # IX[nc,1]=IX[nc,0]+1
      #  IX[nc,2]=IX[nc,1]+(NTH+1) 
     #   IX[nc,3]=IX[nc,0]+(NTH+1)  
# Οριακές Συνθήκες (k=1,...,2*NODES):
    # BC(k) = 0 (ελεύθερος ΒΕ0, k-th DOF)
    # BC(k) = 1 (περιορισμένος ΒΕ, k-th DOF)
# Καταρχήν: όλοι οι Βαθμοί Ελευθερίας (ΒΕ) είναι ελεύθεροι (BC=0)
# Ορίζουμε: μόνο τους περιορισμένους ΒΕ (restrained / fixed) dofs
PSI= np.zeros([NDOFS,1])
PHI= np.zeros([NDOFS,1])
BC[0:3]=1
PSI[0:3]=0
PHI[0:3]=0
BC[10:13]=1
PSI[10:13]=2
BC[4]=1
PSI[4]=X[4,1]
BC[8]=1
PSI[8]=X[8,1]   # κόμβος-1, ο οριζόντιος ΒΕ είναι περιορισμένος
#BC[NY*(NTH+1):(NR+1)*(NTH+1)]=1   # κόμβος-7, εντελώς ακλόνητος (2*7-1 = 13)
# Εξωτερικά Επιβαλόμενες Δυνάμεις (Externally Applied Forces)
# default: όλες οι επιβαλλόμενες δυνάμεις, αρχικά είναι μηδενικές.
# Ορίζουμε: μόνο τις μη-μηδενικές συνιστώσες δυνάμεων
#FI[1:NX] =1000      #κόμβος  6, οριζόντια διεύθυνση (2*6-1 = 11) 
#FI[NY*(NX+1):(NY+1)*(NX+1)] = 10000       #κόμβος 12, οριζόντια διεύθυνση (2*6-1 = 11)  
# Τέλος δεδομένων-----------------------------------------------------------
# ΠΡΟΕΤΟΙΜΑΣΙΑ ΓΙΑ ΤΗΝ ΑΝΑΛΥΣΗ
# Ορισμός μητρώων / μηδενισμός τιμών
#Kglob = sp.lil_matrix((ndofs,ndofs),dtype=float) # Καθολικό Μητρώο Στιβαρότητας
Kloc = np.zeros([NEL,NEL])               # Τοπικό Μητρώο Στιβαρότητας
U  = np.zeros([NDOFS,1])             # Κομβικές Μετατοπίσεις (καθολικές)
DUX = np.zeros([NDOFS,1])            # Μεταβολή οριζ. μετατοπίσεων άκρων
free_dofs = []                       # α/α ελεύθερων Βαθμών Ελευθερίας
fixed_dofs = []                      # α/α περιορισμένων Βαθμών Ελευθερίας
#--------------------------------------------------------------------------
# Σχεδιασμός πλέγματος
MaxCoorX = max(X[:,0]); MinCoorX = min(X[:,0])
MaxCoorY = max(X[:,1]); MinCoorY = min(X[:,1])
dsX = abs(MaxCoorX - MinCoorX)
dsY = abs(MaxCoorY - MinCoorY)
for i in range(NODES):
    plt.annotate(str(i+1),(X[i,0], X[i,1]),xytext=(0.1,0.1),textcoords="offset points",ha='right',color='red',weight='bold',size='medium')
# Ολοκλήρωση διαγράμματος:
plt.title('ΡΟΗ ΡΕΥΣΤΟΥ ΓΥΡΩ ΑΠΟ ΚΥΛΙΝΔΡΟ',style='italic',weight='bold',size='x-large')
plt.xlabel('X')
plt.ylabel('Y')
IX=np.array([[1,2,6],[1,6,5],[5,6,10],[5,10,9],[9,10,12],[9,12,11],[2,3,6],[6,3,7],[6,7,10],[10,7,13],[10,13,12],[3,4,8],[3,8,7],[7,8,14],[7,14,13]])
#plt.show()
#---------------------------------------------------------------------------  
#        ΑΝΑΛΥΣΗ ΠΕΠΕΡΑΣΜΕΝΩΝ ΣΤΟΙΧΕΙΩΝ (FEA ANALYSIS)
# Ορίζουμε το είδος της επίπεδης Ανάλυσης μέσω του 'PlaneType':
#
#---------------------------------------------------------------------------
L = 2      # GAUSS PTS/DIR
KAT = 5    # KAT=2 (AXISYMMETRIC), KAT!=2 (2D-PLANE GEOMETRY)
[Kglob,M,NELE,IX,shp]=MeshGen(X,X,X,NEL,KAT,L,NDF,NDOFS,IX)  
Kglob=Kglob.tocsr()
#print(Kglob)
#rint(Kglob)                  
# Βρίσκουμε τους αύξοντες αριθμούς των ελεύθερων (μη-περιορισμένων) και ...
# ... των περιορισμένων Βαθμών Ελευθερίας:
free_dofs  = np.nonzero(BC-1)[0]    #οι α/α των μη-περιορισμένων ΒΕ
fixed_dofs = np.nonzero(BC)[0]      #οι α/α των περιορισμένων ΒΕ

# Υπολογίζουμε τις άγνωστες κομβικές μετατοπίσεις:

PSI[free_dofs,0]=-spsolve(Kglob[free_dofs,:][:,free_dofs],Kglob[free_dofs,:][:,fixed_dofs]*PSI[fixed_dofs,0])
PHI[free_dofs,0]=-spsolve(Kglob[free_dofs,:][:,free_dofs],Kglob[free_dofs,:][:,fixed_dofs]*PHI[fixed_dofs,0])
circle=plt.Circle((2,0),1,fill=False,color='black')
fig=plt.gcf()
ax=fig.gca()
ax.add_artist(circle)
plt.ylim([-1,2.5])
plt.xlim([0,3])
plt.show()
Vpsi=0
for i in [11,12,13]:
    vx=0
    vy=0
    Vpsil=0
    for k in [0,1,2]:
        ig=IX[i,k]-1
        vx+=shp[1,k]*PSI[ig]
        vy-=shp[0,k]*PSI[ig]
    Vpsil+=vx
    Vpsi+=Vpsil
    print(f'Η μέση ταχύτητα λόγω του στοιχείου {i+1} είναι: {Vpsil[0]:.4f} m/s')
    print(56*'-')
Vpsi=Vpsi/3
print(f'Η μέση ταχύτητα στον κόμβο 8 έιναι: {Vpsi[0]:.4f} m/s')
# ΤΕΛΟΣ ΚΥΡΙΟΥ ΠΡΟΓΡΑΜΜΑΤΟΣ
