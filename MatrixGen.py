


def MatrixGen(X,NEL,KAT,L,NDF,NDOFS,IX,NELE):
    if (NEL==4):
        Kglob = sp.lil_matrix((NDOFS,NDOFS),dtype=float)
        for i in range(NELE):     # Για κάθε τετρακομβικό στοιχείο ελαστικότητας
            ix=IX[i,:]
            XL=np.transpose(X[ix-1,:])
            [Kloc,M] = Matrices_2D_Laplace(XL,KAT,ix,L,NDF,NEL)
            edof=ix-1
            n=0
            for i in edof:
                temp=np.asarray(Kglob[i,:].todense())[0]
                temp[edof]+=Kloc[n,:]
                Kglob[i,:]=temp
                n+=1   
        return([Kglob,M,NELE,IX])