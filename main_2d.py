#--------------------------------------------------------------------------
# FINITE ELEMENT ANALYSIS - 2D Laplace problems         
#                                                       
#               Isoparametric (3-9 nodes)               
#--------------------------------------------------------------------------
# Χρειάζεται τις functions: 1) Matrices_2D_Laplace      
#                           2) PGAUSS                   
#                           3) shape8                   
#--------------------------------------------------------------------------
# fixed_dofs     : Οι περιορισμένοι Βαθμοί Ελευθερίας
# free_dofs      : Οι μη-περιορισμένοι Βαθμοί Ελευθερίας
# F1             : διάνυσμα καθορισμένων τιμών στο σύνορο
#-------------------------------------------------------------------------
import numpy as np
from scipy import sparse as sp
from scipy.sparse.linalg import spsolve
import matplotlib
from matplotlib import pyplot as plt
from PGauss import PGauss
from Matrices_2D_Laplace import Matrices_2D_Laplace
from MeshGen import MeshGen
#===    ΠΡΟ-ΕΠΕΞΕΡΓΑΣΤΗΣ (Δεδομένα του προβλήματος):
NX=8            
NY=8                 
NODES=(NX+1)*(NY+1)      # Πλήθος κόμβων
NEL=4                  # Πλήθος κόμβων ανά στοιχείο
# Ορισμός και προετοιμασία (μηδενισμός) μητρώων και διανυσμάτων:
NDF = 1                     # αριθμός ΒΕ ανά κόμβο (1 DOFs per node)
NDOFS = NODES * NDF         # συνολικός αριθμός ΒΕ
#
X    = np.zeros([NODES,2])              # κομβικές συντεταγμένες (nodal co-ordinates)
BC   = np.zeros(NODES)              # οριακές συνθήκες (boundary conditions)
FI   = np.zeros(NODES,dtype=float)  # externally applied forces 
U    = np.zeros([1,NODES])              # αρχικές μετατοπίσεις
#-------------------------------------------------------------------------   
# Κομβικές συντεταγμένες:
X1=0 
X2=10
Y1=0
Y2=10
dx=float((X2-X1)/NX)
dy=float((Y2-Y1)/NY)
nc=0
for j in range(NY+1):
    y=Y1+j*dy
    for i in range(NX+1):
        x=X1+i*dx
        X[nc,0]=x
        X[nc,1]=y
        nc+=1
 
# Οριακές Συνθήκες (k=1,...,2*NODES):
    # BC(k) = 0 (ελεύθερος ΒΕ, k-th DOF)
    # BC(k) = 1 (περιορισμένος ΒΕ, k-th DOF)
# Καταρχήν: όλοι οι Βαθμοί Ελευθερίας (ΒΕ) είναι ελεύθεροι (BC=0)
# Ορίζουμε: μόνο τους περιορισμένους ΒΕ (restrained / fixed) dofs
BC[0:NX+1]=1    # κόμβος-1, ο οριζόντιος ΒΕ είναι περιορισμένος
#BC[NY*(NTH+1):(NR+1)*(NTH+1)]=1   # κόμβος-7, εντελώς ακλόνητος (2*7-1 = 13)
# Εξωτερικά Επιβαλόμενες Δυνάμεις (Externally Applied Forces)
# default: όλες οι επιβαλλόμενες δυνάμεις, αρχικά είναι μηδενικές.
# Ορίζουμε: μόνο τις μη-μηδενικές συνιστώσες δυνάμεων
FI[1:NX] =1000      #κόμβος  6, οριζόντια διεύθυνση (2*6-1 = 11) 
FI[NY*(NX+1):(NY+1)*(NX+1)] = 10000       #κόμβος 12, οριζόντια διεύθυνση (2*6-1 = 11)  
# Τέλος δεδομένων-----------------------------------------------------------
# ΠΡΟΕΤΟΙΜΑΣΙΑ ΓΙΑ ΤΗΝ ΑΝΑΛΥΣΗ
# Ορισμός μητρώων / μηδενισμός τιμών
Kloc = np.zeros([NEL,NEL])               # Τοπικό Μητρώο Στιβαρότητας
U  = np.zeros([NDOFS,1])             # Κομβικές Μετατοπίσεις (καθολικές)
DUX = np.zeros([NDOFS,1])            # Μεταβολή οριζ. μετατοπίσεων άκρων
free_dofs = []                       # α/α ελεύθερων Βαθμών Ελευθερίας
fixed_dofs = []                      # α/α περιορισμένων Βαθμών Ελευθερίας
#--------------------------------------------------------------------------
# Σχεδιασμός πλέγματος
MaxCoorX = max(X[:,0]); MinCoorX = min(X[:,0])
MaxCoorY = max(X[:,1]); MinCoorY = min(X[:,1])
dsX = abs(MaxCoorX - MinCoorX)
dsY = abs(MaxCoorY - MinCoorY)

# Αρίθμηση κόμβων:
for i in range(NODES):
    plt.annotate(str(i+1),(X[i,0], X[i,1]),xytext=(dsX/100,dsY/100),textcoords="offset points",ha='left',color='red')

# Ολοκλήρωση διαγράμματος:
plt.title('ΤΟΜΗ ΠΑΧΕΩΣ ΚΥΛΙΝΔΡΟΥ',style='italic',weight='bold',size='x-large')
plt.xlabel('X')
plt.ylabel('Y')
#plt.show()
#---------------------------------------------------------------------------  
#        ΑΝΑΛΥΣΗ ΠΕΠΕΡΑΣΜΕΝΩΝ ΣΤΟΙΧΕΙΩΝ (FEA ANALYSIS)
# Ορίζουμε το είδος της επίπεδης Ανάλυσης μέσω του 'PlaneType':
#
#---------------------------------------------------------------------------
L = 2      # GAUSS PTS/DIR
KAT = 5    # KAT=2 (AXISYMMETRIC), KAT!=2 (2D-PLANE GEOMETRY)
[Kglob,M,NELE,IX]=MeshGen(X,NX,NY,NEL,KAT,L,NDF,NDOFS)
#---------------------------------------------------------------------------     
Kglob=Kglob.tocsr()                  
# Βρίσκουμε τους αύξοντες αριθμούς των ελεύθερων (μη-περιορισμένων) και ...
# ... των περιορισμένων Βαθμών Ελευθερίας:
free_dofs  = np.nonzero(BC-1)[0]    #οι α/α των μη-περιορισμένων ΒΕ
fixed_dofs = np.nonzero(BC)[0]      #οι α/α των περιορισμένων ΒΕ
U[free_dofs,0]=-spsolve(Kglob[free_dofs,:][:,free_dofs],Kglob[free_dofs,:][:,fixed_dofs]*FI[fixed_dofs])
print(U)
plt.show()
# ΤΕΛΟΣ ΚΥΡΙΟΥ ΠΡΟΓΡΑΜΜΑΤΟΣ
