#**************************************************************************
#                                                                         *
#  SHAPE DOUBLE PRECISION FUNCTION ROUTINE FOR TWO DIMENSIONAL ELEMENTS   * 
#                                                                         *
#**************************************************************************
import numpy as np
from shape2 import shape2
def shape8(ss,tt,x,NEL,IX):
 NDM=2 #αριθμός διαστάσεων του προβλήματος
 shp=np.zeros([3,9])
 s=[-0.5,0.5,0.5,-0.5]
 t=[-0.5,-0.5,0.5,0.5]
 #s=[-10,10,10,-10]
 #t=[-10,-10,10,10]
#-----form 4-node quadrilateral shape double precision functions
 for i in range(4):
    shp[0,i]=s[i]*(0.5+t[i]*tt)
    shp[1,i]=t[i]*(0.5+s[i]*ss)
    shp[2,i]=(0.5+s[i]*ss)*(0.5+t[i]*tt)
#-----IF NECESSARY FORM TRIANGLE BY ADDING THIRD AND FOURTH TOGETHER  
 if(NEL < 4): 
    for i in range(3):
     shp[i,2]=shp[i,2]+shp[i,3]
#-----ADD QUADRATIC TERMS IF NECESSARY
 if(NEL > 4): 
   shp = shape2(ss,tt,shp,IX,NEL) 
#---------------------------------
#-----CONSTRUCT JACOBIAN AND ITS INVERSE
 xs=np.zeros([NDM,NDM])
 for i in range(NDM):
    for j in range(NDM):
       for k in range(NEL):
        xs[i,j]+=x[i,k]*shp[j,k] 
 sx=np.linalg.inv(xs) # αντίστροφος
#-----FORM GLOBAL DERIVATIVES 
 for i in range(NEL):
    shp[:2,i]=np.dot(shp[:2,i],sx)
 xsj=np.linalg.det(xs)
 return([shp,xsj])
