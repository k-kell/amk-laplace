from Matrices_2D_Laplace_reusto import Matrices_2D_Laplace
import numpy as np
from scipy import sparse as sp
from scipy.sparse.linalg import spsolve
import matplotlib
from matplotlib import pyplot as plt
def MeshGen(X,NR,NTH,NEL,KAT,L,NDF,NDOFS,IX):
 if (NEL==4):
     NELE=14 #NTH*NR 
     #IX= np.zeros([NELE,NEL]).astype('int')
     Kglob = sp.lil_matrix((NDOFS,NDOFS),dtype=float)
     nc=0
     #for j in range(NR):
      #   for i in range(NTH):
      #        IX[nc,0]=j*(NTH+1)+i+1 
      #        IX[nc,1]=IX[nc,0]+1
      #        IX[nc,2]=IX[nc,1]+(NTH+1) 
      #        IX[nc,3]=IX[nc,0]+(NTH+1) 
      #        nc+=1
     xc=np.zeros(NELE)
     yc=np.zeros(NELE)
     nc=0
     for i in range(NELE):
         Xar=[]
         Yar=[]
         for j in IX[i,:]:  
             Xar.append(X[int(j)-1,0])
             Yar.append(X[int(j)-1,1])
         xc[nc]=0.25*sum(Xar)
         yc[nc]=0.25*sum(Yar)
         Xar.append(Xar[0])
         Yar.append(Yar[0])
         plt.plot(Xar,Yar,'b-o',linewidth=1,markersize=4)
         nc+=1
     for i in range(NELE):
         plt.annotate(str(i+1),(xc[i],yc[i]),ha='center',color='black',style='italic',weight='bold')
     for i in range(NELE):     # Για κάθε τετρακομβικό στοιχείο ελαστικότητας
         ix=IX[i,:]
         XL=np.transpose(X[ix-1,:])
         [Kloc,M,shp] = Matrices_2D_Laplace(XL,KAT,ix,L,NDF,NEL)
         edof=ix-1
         n=0
         for i in edof:
             temp=np.asarray(Kglob[i,:].todense())[0]
             temp[edof]+=Kloc[n,:]
             Kglob[i,:]=temp
             n+=1   
     return([Kglob,M,NELE,IX])
 if (NEL==3):
     NELE=15 #2*NTH*NR 
     #IX= np.zeros([NELE,NEL]).astype('int')
     Kglob = sp.lil_matrix((NDOFS,NDOFS),dtype=float)
     nc=0
     #for j in range(NR):
      #   for i in range(NTH):
      #        IX[nc,0]=j*(NTH+1)+i+1 
      #        IX[nc,1]=IX[nc,0]+(NTH+1)+1 
      #        IX[nc,2]=IX[nc,1]-1 
      #        IX[nc+1,0]=IX[nc,0]
      #        IX[nc+1,1]=IX[nc+1,0]+1
      #        IX[nc+1,2]=IX[nc,0]+(NTH+1)+1 
      #        nc+=2 
     xc=np.zeros(NELE)
     yc=np.zeros(NELE)
     nc=0
     for i in range(NELE):
         Xar=[]
         Yar=[]
         for j in IX[i,:]:  
             Xar.append(X[int(j)-1,0])
             Yar.append(X[int(j)-1,1])
         xc[nc]=0.33*sum(Xar)
         yc[nc]=0.33*sum(Yar)
         Xar.append(Xar[0])
         Yar.append(Yar[0])
         plt.plot(Xar,Yar,'b-o',linewidth=1,markersize=3)
         nc+=1
     for i in range(NELE):
         plt.annotate(str(i+1),(xc[i],yc[i]),ha='center',color='black',style='italic',weight='bold',size='small')
     for i in range(NELE):     # Για κάθε τριγωνικό στοιχείο ελαστικότητας
         ix=IX[i,:]
         XL=np.transpose(X[ix-1,:])
         [Kloc,M,shp] = Matrices_2D_Laplace(XL,KAT,ix,L,NDF,NEL)
         edof=ix-1
         n=0
         for i in edof:
             temp=np.asarray(Kglob[i,:].todense())[0]
             temp[edof]+=Kloc[n,:]
             Kglob[i,:]=temp   
             n+=1
     return([Kglob,M,NELE,IX,shp])