#***********************************************************************
#                                                                      *
#      ADD QUADRATIC DOUBLE PRECISION FUNCTIONS AS NECESSARY           * 
#                                                                      *
#***********************************************************************
def shape2(s,t,shp,ix,nel):
 s2=(1-s*s)/2
 t2=(1-t*t)/2
 for i in range(5,nel):
    for j in range(3):
        shp[j,i]=0


#-----midside nodes (serendipity)
 if(ix[4]!=0): 
    shp[0,4]=-s*(1-t)
    shp[1,4]=-s2
    shp[2,4]=s2*(1-t)

 if(ix[5]!=0): 
    shp[0,5]=t2 
    shp[1,5]=-t*(1+s)
    shp[2,5]=t2*(1+s)

 if(ix[6]!=0): 
    shp[0,6]=-s*(1+t)
    shp[1,6]=s2 
    shp[2,6]=s2*(1+t)

 if(ix[7]!=0):

    shp[0,7]=-t2
    shp[1,7]=-t*(1-s)
    shp[2,7]=t2*(1-s)
#-----interior node (lagrangian)
 if(ix[8]!=0): 
    shp[0,8]=-s*t2
    shp[1,8]=-t*s2
    shp[2,8]=4*s2*t2
#-----correct edge nodes for interior node (lagrangian) 
 for j in range(3):
    for i in range(4):
         shp[j,i]=shp[j,i]-0.25*shp[j,7] 
    for i in range(4,8):
        if(ix[i]!=0):
            shp[j,i]=shp[j,i]-0.5*shp[j,8] 
      
#-----correct corner nodes for presense of midside nodes
 k=8
 for i in range(4):
     l=i+4 
     for j in range(3):
         shp[j,i]=shp[j,i]-0.5*(shp[j,k]+shp[j,l])
     k=l
 return(shp)
#-------------------------------------------------------------------------  
