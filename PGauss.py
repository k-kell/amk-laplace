#***********************************************************************
#*                                                                     *
#*         GAUSS POINTS AND WEIGHTS FOR TWO DIMENSIONS                 *
#*                                                                     *
#***********************************************************************
import numpy as np
def  PGauss(L): 
 LR=[-1,1,1,-1,0,1,0,-1,0]
 LZ=[-1,-1,1,1,-1,0,1,0,0]
 LW=[25,25,25,25,40,40,40,40,64] 
 LINT=L*L
 R=np.zeros(L**2)
 Z=np.zeros(L**2)
 W=np.zeros(L**2)
 if(L==1):
#-------1X1 INTEGRATION 
  R[0]=0 
  Z[0]=0 
  W[0]=4 
 elif(L==2):
#-------2X2 INTEGRATION 
    G=1/np.sqrt(3)
    for I in range(4): 
        R[I]=G*LR[I]
        Z[I]=G*LZ[I]
        W[I]=1    
 else:
#-------3X3 INTEGRATION 
    G=np.sqrt(0.6)
    H=1/81
    for I in range(9): 
        R[I]=G*LR[I]
        Z[I]=G*LZ[I]
        W[I]=H*LW[I]
 return([R,Z,W,LINT])
#--------------------------------------------