#***********************************************************************
#*                                                                     *
#*    P L A N E     L I N E A R     E L A S T I C    E L E M E N T     *
#*                         R O U T I N E                               *
#*                                                                     *
#***********************************************************************
from PGauss import PGauss
from shape8 import shape8
import numpy as np
def Matrices_2D_Laplace(XL,KAT,IX,L,NDF,NEL): 
# KAT=2 (AXISYMMETRIC)
# KAT!=2 (2D-PLANE GEOMETRY)
 if(KAT != 2): 
    KAT=1 
 else:
    KAT=2
 [sg,tg,wg,lint] = PGauss(L) # Συντελεστές βαρών
 NDOFS=NDF*NEL # Πλήθος βαθμών ελευθερίας ανά στοιχείο
#-----CALL FAST STIFFNESS AND MASS COMPUTATION , COMPUTE INTEGRALS OF SHAPE DOUBLE PRECISION FUNCTIONS
#-------------LOOP ON ALL ELEMENTS:
 S = np.zeros([NDOFS,NDOFS])
 M = np.zeros([NDOFS,NDOFS])
 for l in range(lint):
     [shp,xsj] = shape8(sg[l],tg[l],XL,NEL,IX)
     if(KAT == 2): 
        rr=0
        for i in range(NEL):
            rr+=shp[2,i]*XL[0,i]
        xsj=xsj*rr       
     for j in range(NEL):
        a1=shp[0,j]*xsj
        a2=shp[1,j]*xsj
        shj=shp[2,j]*xsj
        for i in range(NEL):
            S[i,j]+= a1*shp[0,i]+a2*shp[1,i]
            M[i,j]+= shj*shp[2,i]
 RKTOT=0   #initialize total mass
 RMATOT=0  #initialize total mass
 for I in range(NEL):
 	for J in range(NEL):
 	    RKTOT +=S[I,J]
 	    RMATOT+=M[I,J]
 #print(f'TOTAL STIFFNESS = {RKTOT}')
 #print(f'TOTAL MASS = {RMATOT}') 
 return([S,M])
#------------------------------------      
#anapoda oi synarhseis
