#--------------------------------------------------------------------------
# FINITE ELEMENT ANALYSIS - 2D Laplace problems         
#                                                       
#               Isoparametric (3-4 nodes)               
#--------------------------------------------------------------------------
# Χρειάζεται τις functions: 1) Matrices_2D_Laplace      
#                           2) PGAUSS                   
#                           3) shape8                   
#--------------------------------------------------------------------------
# fixed_dofs     : Οι περιορισμένοι Βαθμοί Ελευθερίας
# free_dofs      : Οι μη-περιορισμένοι Βαθμοί Ελευθερίας
# F1             : διάνυσμα καθορισμένων τιμών στο σύνορο
#-------------------------------------------------------------------------
import numpy as np
from scipy import sparse as sp
from scipy.sparse.linalg import spsolve
import matplotlib
from matplotlib import pyplot as plt
from PGauss import PGauss
from Matrices_2D_Laplace import Matrices_2D_Laplace
from MeshGen import MeshGen
#===    ΠΡΟ-ΕΠΕΞΕΡΓΑΣΤΗΣ (Δεδομένα του προβλήματος):
NTH=10            # Περιφερειακές υποδιαιρέσεις deafult =8
NR=10                 # Ακτινικές υποδιαιρέσεις default = 15
NODES=(NTH+1)*(NR+1)      # Πλήθος κόμβων
NEL=6                # Πλήθος κόμβων ανά στοιχείο
# Ορισμός και προετοιμασία (μηδενισμός) μητρώων και διανυσμάτων:
NDF = 1                     # αριθμός ΒΕ ανά κόμβο (1 DOFs per node)
NDOFS = NODES * NDF         # συνολικός αριθμός ΒΕ

X    = np.zeros([NODES,2])              # κομβικές συντεταγμένες (nodal co-ordinates)
BC   = np.zeros(NODES)              # οριακές συνθήκες (boundary conditions)
FI   = np.zeros(NODES,dtype=float)  # externally applied forces 
U    = np.zeros([1,NODES])              # αρχικές μετατοπίσεις
#-------------------------------------------------------------------------   
# Κομβικές συντεταγμένες:
R1=1
R2=32
dtheta=float((np.pi)/NTH)
dr=float((R2-R1)/NR)
nc=0
for j in range(NR+1):
    r=R1+j*dr
    for i in range(NTH+1):
        theta=np.pi-i*dtheta
        X[nc,0]=r*np.cos(theta)
        X[nc,1]=r*np.sin(theta)
        nc+=1
    
for i in range(NR+1):
    nident=(NTH+1)*i
    plt.plot(X[nident:NTH+1+nident,0],X[nident:NTH+1+nident,1],color='blue',linewidth=1)
plt.grid()
# Οριακές Συνθήκες (k=1,...,2*NODES):
    # BC(k) = 0 (ελεύθερος ΒΕ, k-th DOF)
    # BC(k) = 1 (περιορισμένος ΒΕ, k-th DOF)
# Καταρχήν: όλοι οι Βαθμοί Ελευθερίας (ΒΕ) είναι ελεύθεροι (BC=0)
# Ορίζουμε: μόνο τους περιορισμένους ΒΕ (restrained / fixed) dofs
BC[0:NTH+1]=1    # κόμβος-1, ο οριζόντιος ΒΕ είναι περιορισμένος
BC[NR*(NTH+1):(NR+1)*(NTH+1)]=1   # κόμβος-7, εντελώς ακλόνητος (2*7-1 = 13)
# Εξωτερικά Επιβαλόμενες Δυνάμεις (Externally Applied Forces)
# default: όλες οι επιβαλλόμενες δυνάμεις, αρχικά είναι μηδενικές.
# Ορίζουμε: μόνο τις μη-μηδενικές συνιστώσες δυνάμεων
FI[1:NTH] = 100      #κόμβος  6, οριζόντια διεύθυνση (2*6-1 = 11) 
FI[NR*(NTH+1):(NR+1)*(NTH+1)] = 1000       #κόμβος 12, οριζόντια διεύθυνση (2*6-1 = 11)  
# Τέλος δεδομένων-----------------------------------------------------------
# ΠΡΟΕΤΟΙΜΑΣΙΑ ΓΙΑ ΤΗΝ ΑΝΑΛΥΣΗ
# Ορισμός μητρώων / μηδενισμός τιμών
Kloc = np.zeros([NEL,NEL])               # Τοπικό Μητρώο Στιβαρότητας
U  = np.zeros([NDOFS,1])             # Κομβικές Μετατοπίσεις (καθολικές)
DUX = np.zeros([NDOFS,1])            # Μεταβολή οριζ. μετατοπίσεων άκρων
free_dofs = []                       # α/α ελεύθερων Βαθμών Ελευθερίας
fixed_dofs = []                      # α/α περιορισμένων Βαθμών Ελευθερίας
#--------------------------------------------------------------------------
# Σχεδιασμός πλέγματος
MaxCoorX = max(X[:,0]); MinCoorX = min(X[:,0])
MaxCoorY = max(X[:,1]); MinCoorY = min(X[:,1])
dsX = abs(MaxCoorX - MinCoorX)
dsY = abs(MaxCoorY - MinCoorY)
# Αρίθμηση κόμβων:
for i in range(NODES):
    plt.annotate(f'{i+1}',(X[i,0], X[i,1]),xytext=(dsX/100,dsY/100),textcoords="offset points",ha='left',color='red')

# Ολοκλήρωση διαγράμματος:
plt.title('ΤΟΜΗ ΠΑΧΕΩΣ ΚΥΛΙΝΔΡΟΥ',style='italic',weight='bold',size='x-large')
plt.xlabel('X')
plt.ylabel('Y')
#plt.show()
#---------------------------------------------------------------------------  
#        ΑΝΑΛΥΣΗ ΠΕΠΕΡΑΣΜΕΝΩΝ ΣΤΟΙΧΕΙΩΝ (FEA ANALYSIS)
# Ορίζουμε το είδος της επίπεδης Ανάλυσης μέσω του 'PlaneType':
#
#---------------------------------------------------------------------------
L = 2      # GAUSS PTS/DIR
KAT = 2    # KAT=2 (AXISYMMETRIC), KAT!=2 (2D-PLANE GEOMETRY)
[Kglob,M,NELE,IX]=MeshGen(X,NR,NTH,NEL,KAT,L,NDF,NDOFS)
#---------------------------------------------------------------------------
       
Kglob=Kglob.tocsr()
                 
# Βρίσκουμε τους αύξοντες αριθμούς των ελεύθερων (μη-περιορισμένων) και ...
# ... των περιορισμένων Βαθμών Ελευθερίας:
free_dofs  = np.nonzero(BC-1)[0]    #οι α/α των μη-περιορισμένων ΒΕ
fixed_dofs = np.nonzero(BC)[0]      #οι α/α των περιορισμένων ΒΕ
print(fixed_dofs)
print(Kglob)
# Υπολογίζουμε τις άγνωστες κομβικές μετατοπίσεις:
U[free_dofs,0]=-spsolve(Kglob[free_dofs,:][:,free_dofs],Kglob[free_dofs,:][:,fixed_dofs]*FI[fixed_dofs])
print(U)
plt.show()
# ΤΕΛΟΣ ΚΥΡΙΟΥ ΠΡΟΓΡΑΜΜΑΤΟΣ
